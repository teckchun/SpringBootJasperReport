package com.example.demo.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Car;
import com.example.demo.service.CarService;
import com.example.demo.util.ReportGenerator;

@RestController
public class ReportController {
	@Autowired
	CarService carService;
	
	@GetMapping(value="/print-pdf")
	public ResponseEntity<String> printAllCar(HttpServletRequest request){
		// TODO: define report path and where you store your report
		String filepath = "";
		String UPLOAD_PATH = "";
		
		filepath = request.getServletContext().getRealPath("CarReportPP.jrxml");
		UPLOAD_PATH = "/Users/Teckchun/Documents/reports/";
		
		
		
		// TODO: to check if path exist if not create one
		File path = new File(UPLOAD_PATH);
		
		if(!path.exists()){
			// TODO: create directory
			path.mkdirs();
		}
		
		// TODO: define report name
		String report =  UPLOAD_PATH+"report.pdf";
		
		Map<String,Object> params = new HashMap<String,Object>();
		List<Car> cars = carService.findAll();
		
		params.put("data", cars);
		System.out.println("param=> "+params);
		ReportGenerator.printReport(cars, params, filepath, report);
		return new ResponseEntity<String>("/upload/report.pdf",HttpStatus.OK);	
	}
	
	@GetMapping(value="/print-xls")
	public ResponseEntity<String> printAllCarXls(HttpServletRequest request){
		// TODO: define report path and where you store your report
		String filepath = "";
		String UPLOAD_PATH = "";
		
		filepath = request.getServletContext().getRealPath("CarReportPP.jrxml");
		UPLOAD_PATH = "/Users/Teckchun/Documents/reports/";
		
		
		
		// TODO: to check if path exist if not create one
		File path = new File(UPLOAD_PATH);
		
		if(!path.exists()){
			// TODO: create directory
			path.mkdirs();
		}
		
		// TODO: define report name
		String report =  UPLOAD_PATH+"report.xls";
		
		Map<String,Object> params = new HashMap<String,Object>();
		List<Car> cars = carService.findAll();
		
		params.put("data", cars);
		System.out.println("param=> "+params);
		ReportGenerator.printXLS(cars, params, filepath, report);
		return new ResponseEntity<String>("/upload/report.xls",HttpStatus.OK);	
	}
	@GetMapping(value="/print-one")
	public ResponseEntity<String> printOne(HttpServletRequest request ,
			@RequestParam("carId") int carId){
		// TODO: define report path and where you store your report
		String filepath = "";
		String UPLOAD_PATH = "";
		
		filepath = request.getServletContext().getRealPath("CarReportPP.jrxml");
		UPLOAD_PATH = "/Users/Teckchun/Documents/reports/";
		
		
		
		// TODO: to check if path exist if not create one
		File path = new File(UPLOAD_PATH);
		
		if(!path.exists()){
			// TODO: create directory
			path.mkdirs();
		}
		
		
		
		Map<String,Object> params = new HashMap<String,Object>();
		//List<Car> cars = carService.findAll();
		List<Car> cars = new ArrayList<>();
		Car car = carService.findOne(carId);
		cars.add(car);
		// TODO: define report name
		String report =  UPLOAD_PATH+car.getId()+".pdf";
		
		params.put("data", cars);
		System.out.println("param=> "+params);
		ReportGenerator.printReport(cars, params, filepath, report);
		return new ResponseEntity<String>("/upload/"+car.getId()+".pdf",HttpStatus.OK);	
	}
	
	
	
	
	

}
