package com.example.demo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Car;
import com.example.demo.service.CarService;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private JdbcTemplate jtm;
	
	@Override
	public List<Car> findAll() {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM cars";
		List<Car> cars = jtm.query(sql,new BeanPropertyRowMapper(Car.class)); 
		
		
		return cars;
	}

	@Override
	public Car findOne(int carId) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM cars where ID ="+carId;
		
		Car car = (Car)jtm.queryForObject(sql, new BeanPropertyRowMapper(Car.class));
		
		return car;
	}

}
