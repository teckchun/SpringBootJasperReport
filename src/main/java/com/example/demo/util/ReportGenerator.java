package com.example.demo.util;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

public class ReportGenerator {

	public static boolean printReport(List<?> dataSource, Map<String,Object> params, 
			String reportSourceFile, String pdfExportFile){
	
		
		JasperPrint jasperPrint = null;
		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataSource);
		
		try{
			JasperReport jasperReport= JasperCompileManager.compileReport(reportSourceFile);
			
			jasperPrint = JasperFillManager.fillReport(jasperReport, params, beanColDataSource);
			
			if(jasperPrint!=null){
				JasperExportManager.exportReportToPdfFile(jasperPrint,pdfExportFile);
			}
			return true;
		}catch(JRException e){
			e.printStackTrace();
		}
		return false;
		
	}
	
	public static boolean printXLS(List<?> dataSource, Map<String, Object> params, String reportSourceFile, String xlsExportFile){
		JasperPrint jasperPrint = null;
		JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataSource);
		try {
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSourceFile);
			jasperPrint = JasperFillManager.fillReport(jasperReport, params, beanColDataSource);
			
			if (jasperPrint != null) {
				JRXlsExporter  xlsExporter = new JRXlsExporter ();
				xlsExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, xlsExportFile);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
				xlsExporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.TRUE);
				xlsExporter.exportReport();
			}
			return true;
		} catch (JRException e) {
			e.printStackTrace();
		}
		return false;
	}
}
