package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Car;

public interface CarService {
	public List<Car> findAll();
	public Car findOne(int carId);
}
